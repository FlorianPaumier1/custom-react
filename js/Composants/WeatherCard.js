import Component from "../React/Component.js";
import Cards from "../React/Components/Cards.js";

export default class extends Component{

    constructor() {
        super();
    }

    display(){
        let contentTemp = {tag: "ul", props: {}, children: [
            {tag: 'li', props: {class: ''}, children: ["Température : ", this.state.weather.main.temp]},
            {tag: 'li', props: {class: ''}, children: ["Température Min. : ", this.state.weather.main.temp_min]},
            {tag: 'li', props: {class: ''}, children: ["Température Max. : ", this.state.weather.main.temp_max]},
        ]}

        let contentWeather = {tag: "ul", props: {}, children: [
            {tag: 'li', props: {class: ''}, children: ["Méteo : ", this.state.weather.weather[0].description]},
            {tag: 'li', props: {class: ''}, children: ["Ciel: ", this.state.weather.weather[0].main]},
        ]}

        let contentWind = {tag: "ul", props: {}, children: [
            {tag: 'li', props: {class: ''}, children: ["Vitesse : ", this.state.weather.wind.speed]},
            {tag: 'li', props: {class: ''}, children: ["Degrés: ", this.state.weather.wind.deg]},
        ]}

        return this.dom.createHtmlElement("div", {}, [
            {tag: Cards, props: {title: "Météo à : " + this.state.weather.name, content: [contentTemp, contentWeather, contentWind]}}
        ], this)
    }
}

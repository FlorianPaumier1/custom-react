import Component from "../React/Component.js";
import Header from "./Header.js";
import Footer from "./Footer.js";
import WeatherContent from "./WeatherContent.js";

export default class Weather extends Component {

    constructor() {
        super();
    }

    display() {
        return this.dom.createHtmlElement("div",
            {id: "App"},
            [
                {tag: Header, props: {"title": "React"}},
                {tag: WeatherContent, props: {id: "Wheater"}},
                {tag: Footer, props: {"footer": "Footer"}}
            ],
            this
        )
    }
}

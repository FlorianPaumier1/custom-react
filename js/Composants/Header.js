import Component from "../React/Component.js";
import Link from "../React/Components/Link.js";

export default class Header extends Component {

    constructor(props) {
        super();
        this.receiveData(props)
    }

    display() {
        return this.dom.createHtmlElement("header", {id: "header"},
            [
                {tag: "nav",
                    props: {class: 'navbar navbar-expand-lg navbar-light bg-light'},
                    children: [
                        {
                            tag: Link,
                            props: {
                                path: "/",
                                content: "Mon Site"
                            }
                        },
                        {
                            tag: 'div',
                            props: {
                                class: "collapse navbar-collapse"
                            },
                            children: [
                                {
                                    tag: "ul",
                                    props: {
                                        id: "nav-mobile",
                                        class: "navbar-nav mr-auto"
                                    },
                                    children: [
                                        {
                                            tag: "li", props: {class: 'nav-item active'}, children: [
                                                {tag: Link, props: {path: "/about-us", content: "About Us", class:'nav-link'}}
                                            ]
                                        }, {
                                            tag: "li", props: {class: 'nav-item'}, children: [
                                                {tag: Link, props: {path: "/weather", content: "Météo", class:'nav-link'}}
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ], this)
    }
}


Header.propTypes = {
    text: "string"
}

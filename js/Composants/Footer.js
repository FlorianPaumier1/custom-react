import Component from "../React/Component.js";
import Link from "../React/Components/Link.js";

export default class Footer extends Component {

    constructor() {
        super();
    }

    display() {
        return this.dom.createHtmlElement("footer", {class: 'footer mt-auto py-3 bg-light'}, [
            {
                tag: 'div', props: {class: "container"}, children: [
                    "© 2014 Copyright Text"
                ]
            }
        ], this)
    }
}


Footer.propTypes = {
    text: "string"
}

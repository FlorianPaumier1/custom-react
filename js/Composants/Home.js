import Component from "../React/Component.js";
import Cards from "../React/Components/Cards.js";

export default class Home extends Component {

    constructor(props) {
        super();
        this.receiveData(props)
    }

    display = () => {
        return this.dom.createHtmlElement("div",
            {class: "container", id: this.state.id},
            [
                {tag: 'h1', props: {class: "center-align"}, children: ["Mon Site"]},
                {tag: 'img', props: {src: 'http://newsonair.com/writereaddata/News_Pictures/MIS/2020/May/NPIC-202053171519.jpg'}},
                {tag: 'div', props: {class: 'row justify-content-between'}, children: [
                        {tag: Cards, props: {
                                title: 'Card title',
                                content: 'I am a very simple card. I am good at containing small bits of information. ' +
                                    'I am convenient because I require little markup to use effectively.',
                                links: [
                                    {path: '/', content: "Link 1"},
                                    {path: '/', content: "Link 2"}
                                ]
                            }},
                        {tag: Cards, props: {
                                title: 'Card title',
                                content: 'I am a very simple card. I am good at containing small bits of information. ' +
                                    'I am convenient because I require little markup to use effectively.',
                                links: [
                                    {path: '/', content: "Link 1"},
                                    {path: '/', content: "Link 2"}
                                ]
                            }},
                        {tag: Cards, props: {
                                title: 'Card title',
                                content: 'I am a very simple card. I am good at containing small bits of information. ' +
                                    'I am convenient because I require little markup to use effectively.',
                                links: [
                                    {path: '/', content: "Link 1"},
                                    {path: '/', content: "Link 2"}
                                ]
                            }}
                    ]}
            ],
            this
        )
    }
}


Home.propTypes = {
    text: "string"
}

import Component from "../React/Component.js";
import Footer from "./Footer.js";
import Home from "./Home.js";
import Header from "./Header.js";

export default class Accueil extends Component {

    constructor() {
        super();
    }

    display() {
        return this.dom.createHtmlElement("div",
            {id: "App"},
            [
                {tag: Header, props: {"title": "React"}},
                {tag: Home, props: {id: "Home", text: "Text 3"}, children: "Home"},
                {tag: Footer, props: {"footer": "Footer"}}
                ],
            this
        )
    }
}

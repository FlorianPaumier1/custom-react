import Component from "../React/Component.js";
import WeatherCard from "./WeatherCard.js";

export default class WeatherContent extends Component {

    constructor() {
        super();
        this.getCities()
    }


    display() {
        return this.dom.createHtmlElement("div",
            {id: "App", class: 'container'},
            [
                {tag: 'h1', props: {class: 'text-center'}, children: ["Votre météo"]},
                {
                    tag: 'div', props: {class: 'input-field col s12'}, children: [
                        {tag: 'select', props: {id: 'select-cities', change: () => this.getWeather()}, children: this.state.cities ? this.state.cities : []},
                        {tag: 'label', props: {}, children: ['Ville']}
                    ]
                },
                this.state.weather ? {tag: WeatherCard, props:{weather: this.state.weather}} : null
            ],
            this
        )
    }

    async getCities() {
        let json = await fetch('/js/assets/city-fr.json')
        let cities = await json.json()

        let options = [{
            tag: 'option',
            props: {
                value: "null"
            },
            children: ["choisissez une ville"]
        }]

        cities.forEach((city) => {
            let data = {
                tag: 'option',
                props: {
                    value: city.city
                },
                children: [city.city]
            }

            if (city.city === this.state.city){
                data.props.selected = 'selected'
            }
            options.push(data)
        })
        this.setState({cities: options})
    }

    async getWeather() {
        const city = document.getElementById("select-cities").value

        const response = await fetch(`https://community-open-weather-map.p.rapidapi.com/weather?q=${city},fr&units=metric`, {
            "method": "GET",
            "headers": {
                "x-rapidapi-key": "d408cdd521msh6b8c7a3a15d6edep1378bdjsne7182ed5b602",
                "x-rapidapi-host": "community-open-weather-map.p.rapidapi.com"
            }
        })

        let weather = await response.json();
        this.setState({...this.state, weather: weather, city: city})
    }
}

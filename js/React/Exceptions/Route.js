export const RouteNotFoundException = function(route){

    const instance = new Error(`La route pour l'url "${route}" n'existe pas`)

    if(Error.captureStackTrace)
        Error.captureStackTrace(instance, RouteNotFoundException)

    return instance
}

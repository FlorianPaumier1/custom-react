export const ComponentCantDisplay = function (component) {
    const instance = new Error(`Le component ${component} ne peux pas être afficher car il n'implémente pas la fonction display()`)

    if (Error.captureStackTrace){
        Error.captureStackTrace(instance, ComponentCantDisplay)
    }

    return instance
}
export const PropsInvalid = function (component) {
    const instance = new Error(`Problème lors de l'ajout des paramètres du component ${component}`)

    if (Error.captureStackTrace){
        Error.captureStackTrace(instance, ComponentCantDisplay)
    }

    return instance
}

export const PathNotFound = function (path, obj) {
    const instance = new Error(`Le chemin ${path} n'existe pas dans ${JSON.stringify(obj)}`)

    if (Error.captureStackTrace){
        Error.captureStackTrace(instance, ComponentCantDisplay)
    }

    return instance
}

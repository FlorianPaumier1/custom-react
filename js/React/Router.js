import Dom from "./Render/Dom.js";
import Routing from "../Routing/Routing.js";
import {RouteNotFoundException} from "./Exceptions/Route.js";

export default class Router {

    routes = []
    mode = null
    rootUrl = "/"

    /**
     * @type {React}
     */
    react = null

    /**
     *
     * @type {Dom}
     */
    dom = null

    constructor(react, options = {}) {
        this.mode = window.history.pushState ? "history" : "hash";
        this.history = window.history
        this.react = react

        this.dom = new Dom(react)

        if (options.mode) this.mode = options.mode;
        if (options.root) this.rootUrl = options.root;

        this.listenChange();
        this.loadRoutes()
    }

    /**
     * Add a new route to use
     * @param {string} path
     * @param {string} element
     * @returns {Router}
     */
    add = (path, element) => {
        this.routes.push({path, element});
        return this;
    };

    /**
     * Remove the route link to this path
     * @param {string} path
     * @returns {Router}
     */
    remove = path => {
        for (let i = 0; i < this.routes.length; i += 1) {
            if (this.routes[i].path === path) {
                this.routes.slice(i, 1);
                return this;
            }
        }
        return this;
    };

    /**
     * Clear the routes
     * @returns {Router}
     */
    flush = () => {
        this.routes = [];
        return this;
    };

    /**
     * Remove the first and the last /
     * @param {string} path
     * @returns {string}
     */
    clearSlashes = path =>
        path
            .toString()
            .replace(/\/$/, "")
            .replace(/^\//, "");

    getFragment = () => {
        let fragment = "";

        if (this.mode === "history") {
            fragment = this.clearSlashes(decodeURI(window.location.pathname + window.location.search));
            fragment = fragment.replace(/\?(.*)$/, "");
            fragment = this.rootUrl !== "/" ? fragment.replace(this.rootUrl, "") : fragment;
        } else {
            const match = window.location.href.match(/#(.*)$/);
            fragment = match ? match[1] : "";
        }
        return this.clearSlashes(fragment);
    };

    goTo = (path = "") => {
        if (this.mode === "history") {
            window.history.pushState(null, null, this.rootUrl + this.clearSlashes(path));
        } else {
            window.location.href = `${window.location.href.replace(/#(.*)$/, "")}#${path}`;
        }
        return this;
    }

    listenChange = () => {
        clearInterval(this.interval);
        this.interval = setInterval(this.interval, 50);
    }

    interval = () => {
        if (this.current === this.getFragment()) return;
        this.current = this.getFragment();
        let component = null;

        const hasMatch = this.routes.some(route => {
            const match = this.current === route.path;

            if (match) {
                component = route
            }
            return match;
        });

        if (!hasMatch){
            throw new RouteNotFoundException(this.current)
        }

        this.dom.createDom(component)

        const links = document.querySelectorAll("a.link")
        links.forEach(link => link.addEventListener('click', (e) => {
            e.preventDefault()
            this.goTo(e.currentTarget.getAttribute("href"))
        }))
    };

    previousPage = () => {
        this.history.back()
    }

    nextPage = () => {
        this.history.forward()
    }

    loadRoutes() {
        const routing = new Routing(this.dom)
        routing.routes.forEach(route => this.add(route.path, route))
    }
}

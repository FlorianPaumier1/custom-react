import {events, type_check, type_check_v2} from "../Helpers.js";
import Component from "../Component.js";
import {ComponentCantDisplay} from "../Exceptions/Component.js";

export default class Dom {
    constructor(react = null, component = null) {
        this.react = react
        this.component = component
    }

    _react

    get react() {
        return this._react;
    }

    set react(value) {
        this._react = value;
    }

    _component

    get component() {
        return this._component;
    }

    set component(value) {
        this._component = value;
    }

    _rendered

    get rendered() {
        return this._rendered;
    }

    set rendered(value) {
        this._rendered = value;
    }

    parent

    /**
     *
     * @param {string} tagOrComponent
     * @param {object} props
     * @param {...object|string} children
     * @param parentElement
     * @returns {object}
     */
    createHtmlElement(tagOrComponent, props, children, parentElement = null) {

        let html = {element: null, children: []}

        if (type_check(tagOrComponent, 'string')) {
            let element = document.createElement(tagOrComponent)
            Object.keys(props).map((attribute) => {
                if (events.find(attr => attr === attribute)) {
                    element.addEventListener(attribute, props[attribute])
                } else {
                    element.setAttribute(attribute, props[attribute])
                }
            })

            html.element = element

            if (!type_check(parentElement, 'null')
                && type_check(parentElement.rendered, 'undefined')) {
                parentElement.rendered = html;
            } else {
                parentElement = {element: element, children: []}
            }

            html.parent = parentElement

            html = this.generateChild(children, html);
        } else if (type_check(tagOrComponent, 'function')) {
            html = this.generateElement({tag: tagOrComponent, props, children}, parentElement)
        }

        if (parentElement !== null) {
            parentElement.rendered = html
        }

        return html;
    }

    generateElement(element, parent) {
        let child = null;

        if (type_check(element, 'object')) {
            const {tag, props, children} = element
            if (type_check(tag, 'function') && new tag() instanceof Component) {
                const component = new tag()
                component.dom = this
                component.receiveData(props)
                component.router = this.react.router;
                component.parent = parent

                if (type_check(component.display, 'undefined')) {
                    throw new ComponentCantDisplay(component.constructor.name)
                }
                const view = component.display();

                child = {element: this.parseView(view, component), parent: parent}
            } else if (type_check(tag, 'string')) {
                child = {...this.createHtmlElement(tag, props, children, parent)}
            }
        } else {
            let component = parent.parent
            while (!(component instanceof Component)) {
                if (type_check(component, undefined)) break
                component = component.parent;
            }

            let string = this.parseView(element, component)
            child = {element: document.createTextNode(string), parent: parent}
        }

        return child
    }

    /**
     *
     * @param route
     */
    createDom(route) {
        this.removeDom()
        let view = null;

        if (route.element.component.prototype) {
            const component = new route.element.component()
            if (component instanceof Component) {
                component.receiveData(route.element.props)
                component.dom = this;
                if (component.shouldUpdate()) {
                    view = component.display()
                }
            }
        } else {
            view = route.element.component()
        }

        this.rendered = view
        this.react.commitWork(view)
    }

    updateDom(component){
        this.removeDom()

        // TODO : Voir pourquoi a l'update le Dom garde un ligne
        // Rustine
        component.parent.element.innerHTML = ""
        component.parent.element.innerText = ""

        component.parent.element.childNodes.forEach((ele) => component.parent.element.removeChild(ele))
        component.parent.children.forEach((child) => {
            if (type_check_v2(component, {value: child.element.parent})){
                child.element = component.rendered
                child.parent = component
            }
        })


        this.react.commitWork(this.rendered)
    }

    parseView = (element, component) => {
        if (element.nodeType === 3 || type_check(element, 'string')) {

            let string = element.textContent ? element.textContent : element;

            const regex = /.*?{{(.*?)}}.*?/gm;
            let m;
            let match = [];

            while ((m = regex.exec(string)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }

                match.push(m[1]);
            }


            match.forEach(path => {
                string = string.replace("{{" + path + "}}", component.state.prop_access(path));
            })

            element = string
        } else {
            if (element.childNodes) {
                element.childNodes.forEach(child => element.replaceChild(this.parseView(child, component), child))
            }
        }

        return element;
    }

    generateChild(children, parentElement) {
        if (type_check(children, 'undefined')) return parentElement

        for (let child of children) {
            if (type_check(child, "null")) continue;
            if (type_check(child, "array")) {
                child.forEach(ele => {
                    parentElement.children.push(this.generateElement(ele, parentElement))
                })
            } else {

                let element = this.generateElement(child, parentElement)
                if (parentElement.parent.rendered) {
                    parentElement.parent.rendered.children.push(element)
                } else {
                    parentElement.children.push(element)
                }
            }
        }
        return parentElement;
    }

    removeDom() {
        if (this.react.rootElement.children[0]) this.react.rootElement.removeChild(this.react.rootElement.children[0])
    }
}

let newtUnitOfWork = null

/**
 *
 * @param {IdleDeadline} deadline
 */
const workLoop = function (deadline){
    let shouldYield = false

    while (newtUnitOfWork && !shouldYield){
        newtUnitOfWork = performUnitOfWork(newtUnitOfWork)
        shouldYield = deadline.timeRemaining() < 1
    }

    requestIdleCallback(workloop())
}

const performUnitOfWork = function (newtUnitOfWork){}

requestIdleCallback(workLoop)
import {type_check_v2} from "./Helpers.js";

export default class Component {

    /**
     *
     * @private {object}
     */
    _state = {}

    get state() {
        return this._state;
    }

    set state(value) {
        this._state = value;
    }

    /**
     *
     * @private {object}
     */
    _prevState = {}

    get prevState() {
        return this._prevState;
    }

    set prevState(value) {
        this._prevState = value;
    }

    /**
     *
     * @private {Dom}
     */
    _dom

    get dom() {
        return this._dom;
    }

    set dom(value) {
        this._dom = value;
    }

    /**
     *
     * @private {Router}
     */
    _router

    get router() {
        return this._router;
    }

    set router(value) {
        this._router = value;
    }

    _rendered

    get rendered() {
        return this._rendered;
    }

    set rendered(value) {
        this._rendered = value;
    }

    _parent

    get parent() {
        return this._parent;
    }

    set parent(value) {
        this._parent = value;
    }

    constructor() {
        this._state = null
        this._prevState = null
    }

    receiveData(data) {
        if (Object.type_check(data, this.propTypes)) {
            this.state = data
        } else {
            throw new PropsInvalid(this.constructor.name)
        }
    }

    setState(params) {
        this.prevState = this.state
        this.state = {...params}

        if (this.shouldUpdate()) {
            this.rendered = this.display()
            this.dom.updateDom(this)
        }
    }

    shouldUpdate() {
        return !type_check_v2(this.state, {"value": this.prevState})
    }
}

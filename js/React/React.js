import Router from "./Router.js";
import "./ObjectProto.js"
import "./String.js"
import Component from "./Component.js";

export default class React {

    /**
     * @param {HTMLElement}
     * @private
     */
    _rootElement;

    get rootElement() {
        return this._rootElement;
    }

    set rootElement(value) {
        this._rootElement = value;
    }

    _router;

    get router() {
        return this._router;
    }

    set router(value) {
        this._router = value;
    }

    _dom = []

    get dom() {
        return this._dom;
    }

    set dom(value) {
        this._dom.push(value);
    }

    constructor() {
        this.router = new Router(this)
    }

    /**
     * Ajoute la racine de travail en cours au niveau de dom
     */
    commitWork = (element) => {

        if (!element.children && element instanceof Node) {
            if(element.element){
                return element.element
            }
            return element
        }

        let root = element.element
        console.log(element)
        element.children.forEach(child => {
            if (!child.children) {
                if (child.element) {
                    if (child.element.element) {
                        let childElement = this.commitWork(child.element);
                        root.appendChild(childElement)
                    } else {
                        root.appendChild(child.element)
                    }
                }else{
                    root.appendChild(child)
                }
            } else {
                let childElement = this.commitWork(child)
                root.appendChild(childElement)
            }
        })

        this.rootElement.appendChild(root)
        return root;
    }
}

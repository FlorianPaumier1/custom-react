import Component from "../Component.js";
import Link from "./Link.js";

export default class Cards extends Component {

    constructor() {
        super();
    }

    createFooter() {
        let footer = null;
        if (this.state.links) {
            footer = {
                tag: 'div', props: {class: 'card-footer'}, children: []
            }

            this.state.links.forEach((link) => {
                footer.children.push({
                    tag: Link, props: {content: link.content, path: link.path}
                })
            })
        }

        return footer;
    }


    display() {
        return this.dom.createHtmlElement('div', {class: 'card text-center', style: "width: 18rem;"}, [
                {
                    tag: 'div', props: {class: 'card-body'}, children: [
                        {tag: 'span', props: {class: 'card-title'}, children: [this.state.title]},
                        {tag: 'div', props: {class: 'card-text'}, children: this.state.content}
                    ]
                },
                this.createFooter()
            ],
            this
        )
    }
}

Cards.propTypes = {
    title: {
        type: 'string'
    },
    content: {
        type: 'string'
    },
    links: {
        type: 'array'
    }
}
/*

<div className="row">
    <div className="col s12 m6">
        <div className="card blue-grey darken-1">
            <div className="card-content white-text">
                <span className="card-title">Card Title</span>
                <p>I am a very simple card. I am good at containing small bits of information.
                I am convenient because I require little markup to use effectively.</p>
                </div>
                <div className="card-action">
                <a href="#">This is a link</a>
                <a href="#">This is a link</a>
            </div>
        </div>
    </div>
</div>
 */

import Component from "../Component.js";

export default class Link extends Component {
    display() {
        let classes = "link"
        if (this.state.class) {
            classes += " " + this.state.class
        }

        return this.dom.createHtmlElement(
            "a",
            {
                "href": this.state.path,
                'class': classes
            },
            [...this.state.content]
            , this)
    }
}

Link.propTypes = {
    link: 'string',
    children: 'string',
    class: 'string'
}

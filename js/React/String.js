String.prototype.uc_first = function () {
    let string = this.valueOf()

    return string.charAt(0).toUpperCase() + string.slice(1)
}

String.prototype.capitalize = function () {
    const string = this.valueOf();

    return string
        .toLowerCase()
        .split(" ")
        .map(word => ucfirst(word))
        .join(" ")
}

String.prototype.camel_case = function () {
    const string = this.valueOf();

    return capitalize(string).replace(/\W/g, "");
}

String.prototype.snake_case = function() {
    const string = this.valueOf();

    return string
        .toLowerCase()
        .replace(/(\W)+/g, "-");
}
export const type_check = function (arg, type) {
    switch (typeof arg) {
        case "symbol":
        case "number":
        case "string":
        case "boolean":
        case "undefined":
        case "function":
            return type === typeof arg;
        case "object":
            switch (type) {
                case "null":
                    return arg === null;
                case "array":
                    return Array.isArray(arg);
                default:
                    return arg !== null && !Array.isArray(arg);
            }
    }
}

export const type_check_v2 = function(arg, conf){
    for (let toCheck in conf) {
        switch (toCheck) {
            case 'type':
                if (!type_check(arg, conf.type)) return false;
                break;
            case 'value':
                if (JSON.stringify(arg, refReplacer()) !== JSON.stringify(conf.value, refReplacer())) return false;
                break;
            case 'enum':
                let isFound = false;
                for(value of conf.enum){
                    isFound = type_check_v2(arg, {value: value})
                    if (isFound) break;
                }
                if (!isFound) return false
                break
        }
    }

    return true;
}

export const events = ["click", "hover",'blur', 'focus', 'change', 'close', "open", "scroll", 'touch']

export function refReplacer() {
    let m = new Map(), v= new Map(), init = null;

    return function(field, value) {
        let p= m.get(this) + (Array.isArray(this) ? `[${field}]` : '.' + field);
        let isComplex= value===Object(value)

        if (isComplex) m.set(value, p);

        let pp = v.get(value)||'';
        let path = p.replace(/undefined\.\.?/,'');
        let val = pp ? `#REF:${pp[0]=='[' ? '$':'$.'}${pp}` : value;

        !init ? (init=value) : (val===init ? val="#REF:$" : 0);
        if(!pp && isComplex) v.set(value, path);

        return val;
    }
}

import Accueil from "../Composants/Accueil.js";
import Weather from "../Composants/Weather.js";

export default class Routing {
    constructor(dom) {
        this.dom = dom
    }


    routes = [
        {
            "component": Accueil,
            "path": "",
            props: {text: "Ceci est un text"}
        }, {
            "component": Weather,
            "path": "weather"
        },
        {component: () => this.dom.createHtmlElement("div", {}, ["About Us"]), path: "about-us"}
    ]

}

# Consigne
## Créer un mini-react

- Gestion du routage &#10003;

- Validation des propriétés passées au composant &#10003;

- Algorithme => se rapproche de l'algo de l'exercice *HereWeGo!*

- Chaque composant hérite d'un object `Component` ayant une méthode `display(newProps)` &#10003;

- display appelle la méthode `shouldUpdate()` du composant courant => compare newProps avec les
  oldProps

    - si shouldUpdate

        - appelle la fonction `render` du composant &#10003;

        - si `render` invoque d'autres composants, le composant courant appelle la fonction `display
  (compProps)` des sous-composants &#10003;

    - le résultat de `display` est ajouté au DOM sous le noeud parent &#10003;

## Créer un mini site de démo (2/3 pages)

Utilisation du routing et du mini-re

#Obligations

Norme Javascript: ES6/ES2015

Notions présentes:

- Prototypes d'objet natif (String, Object, Number, ...)

- Object.prop_access avec exception

- String.interpolate(animal)

- remplace toutes les chaines entourées de "{{ }}" par la valeur de l'objet

- machaine = "Type d'animal: {{ type.name }}"

- animal = {type: {name: "chien"}}

- machaine.interpolate(animal) => "Type d'animal: chien"

- Création d'objet et objet hérité dont certains avec attributs/méthodes privés

- Création de modules

- Gestion de l'historique (système de routage)

- Utilisation des Promises

- Utilisation du type_checker

- version minimum: 3

- exemples cas d'utilisation: Vérifier les données en entrée de constructeur

Contenu index.html:

 <html>

 <head>

...

 <script type="module" src="./main.js"/>

 </head>

 <body>

 <div id="root"></div>

</body>

 </html>

Interdictions

 - Utilisation de task-runners sauf compilateur SASS

 - Utilisation de Framework/Librairies (React, Angular, VueJS, jQuery, ...) sauf CSS (TailwindCSS,
Bootstrap CSS, ...)

